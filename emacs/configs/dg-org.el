;; Configuration for Org Mode
(provide 'dg-org)

;;Load relevant modeules here

(require 'ox-beamer)
(require 'ox-latex)
(require 'ox-reveal)

;;Babel highlights
(setq org-src-fontify-natively t)

;;Flyspell
;;(add-hook 'org-mode-hook 'flyspell-mode)
;;(add-hook 'org-mode-hook 'flyspell-buffer)

(setq org-latex-pdf-process (quote ("texi2dvi --pdf --clean --verbose --batch %f")))
