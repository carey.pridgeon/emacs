;; ------ GENERAL CONFIG FOR EMACS

(provide 'dg-general)

(setq inhibit-startup-screen t)
(setq transient-mark-mode t)

(fset 'yes-or-no-p 'y-or-n-p)

;;Line and Columns
(setq line-number-mode t)
(setq column-number-mode t)
;;(toggle-scroll-bar t)
(tool-bar-mode -1)
;; Line wrap at 80
(set-default 'fill-column 80)
